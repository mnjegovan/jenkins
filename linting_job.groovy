node{
  stage("Clone repository"){
    git "https://gitlab.com/mnjegovan/python-movies.git"
  }
  stage("pyLint"){
    sh "pylint * --output-format=html > output.html"
  }
  stage("Report"){
    publishHTML([
      allowMissing: false,
      alwaysLinkToLastBuild: false,
      keepAll: true,
      reportDir: '',
      reportFiles: 'output.html',
      reportName: 'Pylint Report'])
  }
}